#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

F90FLAGS := $(shell dpkg-buildflags --get F90LAGS)
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

F90FLAGS += -O3

VERSION=$(strip $(shell \
		head -n 1 debian/changelog | cut -f 2 -d ' ' | tr -d '()' | sed -e 's/\+.*$$//'))

pythia-$(VERSION).f:
	dh_testdir
	wget http://www.hepforge.org/archive/pythia6/pythia-$(VERSION).f.gz
	gunzip pythia-$(VERSION).f.gz

libpythia6.a: pythia-$(VERSION).f
	dh_testdir
	gfortran $(F90FLAGS) -c $^
	ar r libpythia6.a *.o
	ranlib libpythia6.a

build: libpythia6.a

install-arch:
	dh_testdir
	dh_prep
	mkdir -p debian/libpythia6-dev/usr/lib/$(DEB_HOST_MULTIARCH)
	cp -a libpythia6.a debian/libpythia6-dev/usr/lib/$(DEB_HOST_MULTIARCH)

binary-arch: install-arch
	dh $@

binary: binary-arch

clean:
	dh_testdir
	dh_auto_clean
	dh_clean
	-rm -f *.f *.gz *.a *.o

%:
	dh $@ 

.PHONY: build install-arch binary binary-arch
